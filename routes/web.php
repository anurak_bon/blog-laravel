<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'PostController@index');
// Create follow this steps.
// Controller => PostController => php artisan make:controller PostsController
// Eloquent model => Post => php artisan make:model Post
// migration => create.posts.table => php artisan make:migration create_posts_table --create-posts
// Also Short command: php artisan make:model Post -mc

